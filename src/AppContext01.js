import React, { createContext, useContext } from 'react';

const ThemeContext = createContext({bg:'green'});

const AppContext01 = () => {
    const theme = useContext(ThemeContext);
    const style = {
        width: '24px',
        height: '24px',
        background: theme.bg
    };
    return (
        <div>
            <div style={style} />
            <div style={{width:'24px', height:'24px', backgroundColor:'blue'}} />
        </div>
    );
};

export default AppContext01;