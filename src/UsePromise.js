import React from 'react';
import usePromiseFn from './usePromiseFn';

const wait = () => {
    // 3초 후에 끝나는 프로미스를 반환
    return new Promise(resolve =>
        setTimeout(() => resolve('Hello hooks!'), 3000)
    );
};
const UsePromise = () => {
    const [loading, resolved, error] = usePromiseFn(wait, []);

    if (loading) return <div>로딩중..!</div>;
    if (error) return <div>에러 발생!</div>;
    if (!resolved) return null;

    return <div>{resolved}</div>;
};

export default UsePromise;