import React, { useRef } from 'react';

const UseRefLocalVariable = () => {
    const id = useRef(1);
    const setId = (n) => {
        id.current = n;
    }
    const printId = () => {
        console.log(id.current);
    }

    //printId();

    return (
        <div>
            refsample
        </div>
    );
};

export default UseRefLocalVariable;