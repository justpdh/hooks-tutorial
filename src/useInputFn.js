import { useReducer } from 'react';

function reducer(state, action) {
    return {
        ...state,
        [action.name]: action.value
    };
}

export default function useInputFn(initialFormData) {
    const [state, dispatch] = useReducer(reducer, initialFormData);
    const onChange = e => {
        dispatch(e.target);
    };
    return [state, onChange];
}