import logo from './logo.svg';
import './App.css';

import React, { useState } from 'react';
import Counter from './Counter';
import UseEffectBefore from './UseEffectBefore';
import AppContext01 from './AppContext01';
import AppContext02 from './AppContext02';
import CounterReducer from './CounterReducer';
import InputStateManage from './InputStateManage';
import UseMemoNone from './UseMemoNone';
import UseMemo from './UseMemo';
import UseCallback from './UseCallback';
import UseRef from './UseRef';
import UseRefLocalVariable from './UseRefLocalVariable';
import UseInput from './UseInput';
import UsePromise from './UsePromise';



/* 
const App = () => {
    return (
        <Counter />
    );
};
 */

/* 
const App = () => {
    const [visible, setVisible] = useState(false);

    return (
        <div>
            <button onClick={() => { setVisible(!visible); }} >{visible ? '숨기기' : '보이기'}</button>
            <hr />
            {visible && <UseEffectBefore />}
        </div>
    );
};
 */


/* 
const App = () => {
    return (
        <AppContext01 />
    );
};
 */

/* 
const App = () => {
    return (
        <AppContext02 />
    );
};
 */

/*
const App = () => {
    return (
        <CounterReducer />
    );
};
*/

/*
const App = () => {
    return (
        <InputStateManage />
    );
};
*/
/* 
const App = () => {
    return (
        <UseMemoNone />
    );
};
 */

/*
const App = () => {
    return (
        <UseMemo />
    );
};
*/

/*
const App = () => {
    return (
        <UseCallback />
    );
};
*/

/*
const App = () => {
    return (
        <UseRef />
    );
};
*/
/*
const App = () => {
    return (
        <UseRefLocalVariable />
    );
};
*/
/*
const App = () => {
    return (
        <UseInput />
    );
};
*/


const App = () => {
    return (
        <UsePromise />
    );
};


export default App;
